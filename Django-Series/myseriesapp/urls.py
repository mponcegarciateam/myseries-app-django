from django.conf.urls import url, include
from django.contrib.auth.views import login,logout
from myseriesapp import views
from myseriesapp.views import Animes, Series, Productoras

urlpatterns = [
    url(r'series/', Series.as_view(), name='series'),
    url(r'animes/', Animes.as_view(), name='animes'),
    url(r'^inicio/', views.inicio, name='inicio'),
    url(r'productoras/', Productoras.as_view(), name='productoras'),
    url(r'^perfil/$', views.perfil, name='perfil'),
    url(r'^login$',login, {'template_name' : 'login.html', }, name='login'),
    url(r'^logout$',logout, {'next_page': 'myseriesapp:inicio', }, name='logout'),
    url(r'^signup$', views.signup, name='Registrarse'),
    url(r'^detalle/serie/(?P<serie_id>\d+)/$', views.serie_nombre,name='serie_nombre'),
    url(r'^detalle/anime/(?P<anime_id>\d+)/$', views.anime_nombre,name='anime_nombre'),
    url(r'registro_serie/$', views.register_serie, name='registro_serie'),
    url(r'registro_anime/$', views.register_anime, name='registro_anime'),
    url(r'registro_productora/$', views.register_productora, name='registro_productora'),
]