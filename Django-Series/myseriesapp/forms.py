
from django import forms
from myseriesapp.models import Serie, Productora, Anime
from ckeditor.fields import RichTextField

class SerieForm(forms.Form):
    nombre=forms.CharField(max_length=50,label='Nombre')
    año_lanzamiento=forms.DateField(widget=forms.SelectDateWidget(years=range(1950,2017)))
    descripcion=forms.CharField(max_length=5000,label='descripcion')
    imagen=forms.ImageField(widget=forms.FileInput,required=False)
    productora=forms.ModelChoiceField(queryset=Productora.objects.all())

class AnimeForm(forms.Form):
    nombre=forms.CharField(max_length=50,label='Nombre')
    año_lanzamiento=forms.DateField(widget=forms.SelectDateWidget(years=range(1960,2017)))
    descripcion=forms.CharField(max_length=5000,label='descripcion')
    imagen=forms.ImageField(widget=forms.FileInput,required=False)
    productora=forms.ModelChoiceField(queryset=Productora.objects.all())

class ProductoraForm(forms.Form):
    nombre=forms.CharField(max_length=50,label='Nombre')
    año_fundacion=forms.DateField(widget=forms.SelectDateWidget(years=range(1920,2017)))
    web=forms.CharField(max_length=100,label='web')

class SignUpForm(forms.Form):
    username=forms.CharField(max_length=30,label='Nombre de usuario')
    password=forms.CharField(max_length=30,label='Contraseña', widget=forms.PasswordInput())
    email=forms.CharField(max_length=50,label='Correo electrónico')
    first_name=forms.CharField(max_length=30,label='Nombre')
    last_name=forms.CharField(max_length=30,label='Apellido')

class PerfilImagenForm(forms.Form):
    imagen=forms.ImageField(widget=forms.FileInput,required=False)

class RateSerieForm(forms.Form):
    nota=forms.FloatField()

    def clean_nota(self):
        if self.cleaned_data['nota'] >= 10 or self.cleaned_data['nota'] <= 0:
            raise forms.ValidationError("La nota tiene que estar entre 0 y 10")
        return self.cleaned_data['nota']

class RateAnimeForm(forms.Form):
    nota=forms.FloatField()
    def clean_nota(self):
        if self.cleaned_data['nota'] >= 10 or self.cleaned_data['nota'] <= 0:
            raise forms.ValidationError("La nota tiene que estar entre 0 y 10")
        return self.cleaned_data['nota']

class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True, label="Nombre")
    contact_email = forms.EmailField(required=True, label="Email")
    content = forms.CharField(required=True,widget=forms.Textarea, label="Mensaje"
    )