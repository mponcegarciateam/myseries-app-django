# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-01 15:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('myseriesapp', '0007_auto_20170201_1618'),
    ]

    operations = [
        migrations.CreateModel(
            name='Anime',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('año_lanzamiento', models.DateField()),
                ('descripcion', models.CharField(max_length=300)),
                ('imagen', models.ImageField(upload_to='series/')),
                ('puntuacion', models.FloatField(verbose_name=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9))),
                ('productora', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='myseriesapp.Productora')),
            ],
        ),
    ]
