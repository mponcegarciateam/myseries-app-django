# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-08 11:41
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myseriesapp', '0017_auto_20170207_1138'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='anime',
            name='puntuacion',
        ),
        migrations.RemoveField(
            model_name='serie',
            name='puntuacion',
        ),
        migrations.AlterField(
            model_name='anime',
            name='descripcion',
            field=ckeditor.fields.RichTextField(max_length=5000),
        ),
        migrations.AlterField(
            model_name='serie',
            name='descripcion',
            field=ckeditor.fields.RichTextField(max_length=5000),
        ),
    ]
