from django.contrib import admin
from myseriesapp.models import Productora, Serie, Anime, Nota_serie, Nota_anime, User_image
from django import forms

# Register your models here.
class ProductoraAdminForm(forms.ModelForm):
	def clean_web(self):
		if self.cleaned_data["web"][:4] != "http":
			raise forms.ValidationError("La web debe empezar por http://")
		else:
			return self.cleaned_data["web"]

class SerieInLine(admin.StackedInline):
	model = Serie
	readonly_fields = ["nombre"]
	verbose_name = 'Serie'
	verbose_name_plural = 'Lista de Series'

class ProductoraAdmin(admin.ModelAdmin):
	form = ProductoraAdminForm
	list_per_page=5
	inlines=[SerieInLine,]

class SerieAdmin(admin.ModelAdmin):
	list_filter = ['nombre']
	list_per_page=10

class AnimeAdmin(admin.ModelAdmin):
	list_filter = ['nombre']
	list_per_page=10

admin.site.register(Productora, ProductoraAdmin)
admin.site.register(Serie, SerieAdmin)
admin.site.register(Anime, AnimeAdmin)
admin.site.register(Nota_serie)
admin.site.register(Nota_anime)
admin.site.register(User_image)
