from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

# Create your models here.
class Productora(models.Model):
	nombre=models.CharField(max_length=50)
	año_fundacion=models.DateField()
	web=models.CharField(max_length=100)
	def __str__(self):
		return self.nombre

class Serie(models.Model):
	nombre=models.CharField(max_length=50, unique=True)
	año_lanzamiento=models.DateField()
	descripcion=RichTextField(max_length=5000)
	imagen=models.ImageField(upload_to='series/')
	productora=models.ForeignKey(Productora,null=True)
	def __str__(self):
		return self.nombre + ", " + str(self.año_lanzamiento)

class Anime(models.Model):
	nombre=models.CharField(max_length=50, unique=True)
	año_lanzamiento=models.DateField()
	descripcion=RichTextField(max_length=5000)
	imagen=models.ImageField(upload_to='animes/')
	productora=models.ForeignKey(Productora,null=True)
	def __str__(self):
		return self.nombre + ", " + str(self.año_lanzamiento)

class User_image(models.Model):
	usuario=models.OneToOneField(User,null=False,on_delete=models.CASCADE, primary_key=True)
	imagen=models.ImageField(upload_to='user_images/')
	def __str__(self):
		return "imagen de " + self.usuario.username

class Nota_serie(models.Model):
	usuario=models.ForeignKey(User,null=False, on_delete=models.CASCADE)
	serie=models.ForeignKey(Serie,null=False, on_delete=models.CASCADE)
	nota=models.FloatField()
	def __str__(self):
		return self.usuario + " " + self.serie.nombre + ": " + str(self.puntuacion)

class Nota_anime(models.Model):
	class Meta:
		unique_together = ('usuario','anime',)
	usuario=models.ForeignKey(User,null=False, on_delete=models.CASCADE)
	anime=models.ForeignKey(Anime,null=False, on_delete=models.CASCADE)
	nota=models.FloatField()
	def __str__(self):
		return self.usuario.username + " " + self.anime.nombre + ": " + str(self.nota)

