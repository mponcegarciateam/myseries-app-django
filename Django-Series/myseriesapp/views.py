from django.shortcuts import render, render_to_response
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
import datetime
from django.template.context import RequestContext
from myseriesapp.models import Serie, Productora, Anime, Nota_anime,Nota_serie,User_image
from myseriesapp.forms import SerieForm, ProductoraForm, SignUpForm, AnimeForm, RateSerieForm, RateAnimeForm, PerfilImagenForm, ContactForm
from django.views.generic import ListView
from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template import Context
from django.template.loader import get_template

# Create your views here.
class Series(ListView):
	model=Serie
	template_name ='series.html'

class Animes(ListView):
    model=Anime
    template_name='animes.html'

class Productoras(ListView):
    model=Productora
    template_name ='productoras.html'

def serie_nombre(request, serie_id):
    serie=Serie.objects.get(pk=serie_id)
    if request.method == 'POST':
        form = RateSerieForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            usuario = request.user
            nota=cd['nota']
            registro = Nota_serie(usuario=usuario,serie=serie, nota=nota)
            try:
                registro.save()
            except:
                registro=Nota_serie.objects.filter(usuario=usuario,anime=anime).update(nota=nota)
            return HttpResponseRedirect('../'+serie_id)
    else:
        form = RateSerieForm()
    try:
        notas_serie=Nota_serie.objects.filter(serie=serie)
        suma=0
        cont=0
        for nota in notas_serie:
            cont+=1
            suma=suma+nota.nota
        if cont > 0:
            nota_serie=suma/cont
        else:
            nota_serie="Aún no hay puntuaciones"
    except Serie.DoesNotExist:
        raise Http404("La serie no existe")
    context={'serie' : serie, 'nota' : nota_serie, 'form' : form}
    return render(request,"detalle_serie.html",context)

def anime_nombre(request, anime_id):
    anime=Anime.objects.get(pk=anime_id)
    if request.method == 'POST':
        form = RateAnimeForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            usuario = request.user
            nota=cd['nota']
            registro = Nota_anime(usuario=usuario,anime=anime, nota=nota)
            try:
                registro.save()
            except:
                registro=Nota_anime.objects.filter(usuario=usuario,anime=anime).update(nota=nota)
            return HttpResponseRedirect('../'+anime_id)
    else:
        form = RateAnimeForm()
    try:
        notas_anime=Nota_anime.objects.filter(anime=anime)
        suma=0
        cont=0
        for nota in notas_anime:
            cont+=1
            suma=suma+nota.nota
        if cont > 0:
            nota_anime=suma/cont
        else:
            nota_anime="Aún no hay puntuaciones"
    except Anime.DoesNotExist:
        raise Http404("El anime no existe")

    context={'anime' : anime, 'nota' : nota_anime, 'form' : form}
    return render(request,"detalle_anime.html",context)

def register_serie(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = SerieForm(request.POST, request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                nombre=cd['nombre']
                año_lanzamiento=cd['año_lanzamiento']
                descripcion=cd['descripcion']
                imagen=cd['imagen']
                productora=cd['productora']
                serie = Serie(nombre=nombre,año_lanzamiento=año_lanzamiento,descripcion=descripcion,imagen=imagen, productora=productora)
                serie.save()
                return HttpResponseRedirect('../series')
        else:
            form = SerieForm()
        return render(request, 'registro.html', {'form': form, 'type' : 'serie'})
    else:
        return HttpResponseRedirect(reverse('myseriesapp:login'))


def register_anime(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = AnimeForm(request.POST, request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                nombre=cd['nombre']
                año_lanzamiento=cd['año_lanzamiento']
                descripcion=cd['descripcion']
                imagen=cd['imagen']
                productora=cd['productora']
                anime = Anime(nombre=nombre,año_lanzamiento=año_lanzamiento,descripcion=descripcion,imagen=imagen, productora=productora)
                anime.save()
                return HttpResponseRedirect('../animes/')
        else:
            form = AnimeForm()
        return render(request, 'registro.html', {'form': form, 'type' : 'anime'})
    else:
        return HttpResponseRedirect(reverse('myseriesapp:login'))


def register_productora(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = ProductoraForm(request.POST, request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                nombre=cd['nombre']
                año_fundacion=cd['año_fundacion']
                web=cd['web']
                productora = Productora(nombre=nombre,año_fundacion=año_fundacion,web=web)
                productora.save()
                return HttpResponseRedirect('../productoras/')
        else:
            form = ProductoraForm()
        return render(request, 'registro.html', {'form': form})
    else:
        return HttpResponseRedirect(reverse('myseriesapp:login'))

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            username = cd["username"]
            password = cd["password"]
            email = cd["email"]
            first_name = cd["first_name"]
            last_name = cd["last_name"]
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            return HttpResponseRedirect(reverse('myseriesapp:login'))
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def perfil(request):
    if request.user.is_authenticated():
        usuario = request.user
        if request.method == 'POST':
            form = PerfilImagenForm(request.POST, request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                imagen = cd['imagen']
                user_image=User_image(usuario=usuario,imagen=imagen)
                user_image.save()
                return HttpResponseRedirect(reverse('myseriesapp:perfil'))
        else:
            form = PerfilImagenForm()
        try:
            user_image = User_image.objects.get(usuario=usuario)
            context={'usuario' : usuario, 'form' : form, 'imagen' : user_image.imagen}
        except:
            context={'usuario' : usuario, 'form' : form}
        
        return render(request,"perfil.html",context)
    else:
        return HttpResponseRedirect(reverse('myseriesapp:login'))

def inicio(request):
    form_class = ContactForm
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
            , '')
            contact_email = request.POST.get(
                'contact_email'
            , '')
            form_content = request.POST.get('content', '')

            # Email the profile with the 
            # contact information
            template = get_template('contact_template.txt')
            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            })
            content = template.render(context)

            email = EmailMessage(
                "Mensaje de contacto",
                content,
                "Myseriesapp" +'',
                ['myseriesapp@gmail.com'],
                headers = {'Reply-To': contact_email }
            )
            email.send()
            return redirect('myseriesapp:inicio')
    return render(request, 'index.html', {
        'form': form_class,
    })